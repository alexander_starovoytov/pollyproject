﻿using PollyRetry;
using System;

namespace ConsolePolly
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write($"Привет");

            var s3ObjectIndexer = new S3ObjectIndexer();

            var result = s3ObjectIndexer.GetObject(3).Result;

            Console.WriteLine($"Количество элементов {result.Count}");
            Console.ReadKey();
        }
    }
}
