﻿using NUnit.Framework;
using PollyRetry;

namespace PollyRetryTests
{
    [TestFixture]
    public class S3ObjectIndexerTests
    {
        [Test]
        public void GetObject_WithFailed_ShouldReturnResultAfterThirdAttempt()
        {
            // Arrange
            var s3ObjectIndexer = new S3ObjectIndexer();

            // Act
            var result = s3ObjectIndexer.GetObject(2).Result;

            //Assert
            Assert.That(result.Count, Is.EqualTo(8));
        }
    }
}
