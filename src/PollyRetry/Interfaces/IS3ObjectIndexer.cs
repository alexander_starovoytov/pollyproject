﻿using PollyRetry.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PollyRetry.Interfaces
{
    public interface IS3ObjectIndexer
    {
        Task<List<S3Object>> GetObject(int successfulAttept);
    }
}
