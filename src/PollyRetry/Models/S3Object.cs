﻿using System;

namespace PollyRetry.Models
{
    public class S3Object
    {
        public S3Object(string key)
        {
            Key = key;
            ETag = Guid.NewGuid().ToString();
        }

        public string Key { get; }
        public string ETag { get; }
    }
}
