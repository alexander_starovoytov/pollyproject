﻿using PollyRetry.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PollyRetry
{
    public class S3Loader
    {
        private readonly List<S3Object> list;
        private int countAttempt = 0;

        public S3Loader()
        {
            list = new List<S3Object>();
            SetupList();
        }

        public async Task<List<S3Object>> S3LoadObject(int successfulAttempt)
        {
            countAttempt++;
            if (countAttempt < successfulAttempt)
            {
                throw new ArgumentOutOfRangeException();
            }

            return await Task.Run(() => CallToS3());
        }

        private List<S3Object> CallToS3()
        {
            Thread.Sleep(10000);
            return list;
        }

        private void SetupList()
        {
            list.Add(new S3Object("a"));
            list.Add(new S3Object("b"));
            list.Add(new S3Object("c"));
            list.Add(new S3Object("d"));
            list.Add(new S3Object("A"));
            list.Add(new S3Object("b"));
            list.Add(new S3Object("e"));
            list.Add(new S3Object("12"));
        }
    }
}
