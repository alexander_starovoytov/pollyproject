﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Polly;
using PollyRetry.Interfaces;
using PollyRetry.Models;

namespace PollyRetry
{
    public class S3ObjectIndexer : IS3ObjectIndexer
    {
        private const int RETRY_COUNT = 2;
        private readonly S3Loader s3Loader = new S3Loader();
        public int RetryNumber { get; private set; }
        public Exception ReturnedException { get; private set; }

        public async Task<List<S3Object>> GetObject(int successfulAttempt)
        {
            var retryPolicy = Policy
                            .Handle<Exception>()
                            .RetryAsync(RETRY_COUNT, (exception, retryNumber) =>
                            {
                                ReturnedException = exception;
                                RetryNumber = retryNumber;
                            });

            var policyResult = await retryPolicy.ExecuteAndCaptureAsync(() => s3Loader.S3LoadObject(successfulAttempt));

            if (policyResult.Outcome == OutcomeType.Failure)
            {
                throw policyResult.FinalException;
            }

            return policyResult.Result;
        }
    }
}
